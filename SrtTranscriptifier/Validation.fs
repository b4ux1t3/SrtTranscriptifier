namespace SrtTranscriptifier

module Validation =

    open System
    open System.Text.RegularExpressions
    open Types

    let fileIsSrt (filename: string) =
        filename.EndsWith("srt", StringComparison.OrdinalIgnoreCase)
        
    let lineIsSrtMarker (line: string) =
        Regex.IsMatch(line, @"^(?:\d+|(?:\d+:\d+:\d+,\d+\s+-->\s+\d+:\d+:\d+,\d+))$")
        
    let isValidLine str = (str |> String.IsNullOrEmpty |> not) && (str |> lineIsSrtMarker |> not)

    let rec getValidLines (str: ArrayOrString) =
        match str with
        | Array arr -> arr |> Array.filter isValidLine
        | String s -> s.Split('\n') |> Array |> getValidLines
 