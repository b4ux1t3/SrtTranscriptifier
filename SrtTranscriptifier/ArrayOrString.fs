namespace SrtTranscriptifier
module Types =
    type ArrayOrString =
        | Array of string array
        | String of string