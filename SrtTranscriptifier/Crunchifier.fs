namespace SrtTranscriptifier
module Crunchification =
    open Types
    open Validation
    let crunchString str = 
            let lines = str |> String |> getValidLines
            System.String.Join(" ", lines)