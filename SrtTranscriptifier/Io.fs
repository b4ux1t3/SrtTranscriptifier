namespace SrtTranscriptifier


module Io =
    open Crunchification
    open System.IO
    let tryOpenFile filepath =
        try
            File.Open(filepath, FileMode.OpenOrCreate) |> Some
        with
        | :? IOException -> None
        | _ -> None

    let getOutputFile (args: string array) =
        match args with
        | [| outputFilePath |] -> outputFilePath |> tryOpenFile
        | [| outputFilePath ; _ |] -> outputFilePath |> tryOpenFile
        | _ -> None
        
    let getCrunchedLines args =
        match args with
        | [| _ |] -> stdin.ReadToEnd() |> crunchString |> Some
        | [| _; inputFile |] -> File.ReadAllText(inputFile) |> crunchString |> Some
        | _ -> None
    
    let writeToFile (outputFile: FileStream) text =
        let sw = new StreamWriter(outputFile)
        sw.Write(sprintf "%s\n" text)
        printfn $"Transcription written to %s{outputFile.Name}"
        sw.Flush()