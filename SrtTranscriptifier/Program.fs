﻿namespace SrtTranscriptifier
module Main =
    open Io
    [<EntryPoint>]
    let main args =
        let outputFileResult = getOutputFile args
        let crunchedLinesResult = getCrunchedLines args
        
        match outputFileResult, crunchedLinesResult with
        | Some outputFile, Some crunchedLines ->
            writeToFile outputFile crunchedLines
            0
        | _  ->
            printfn "Please provide an output file and, optionally, an input file.\nIf no input file is passed, this program assumes you're passing it through STDIN."
            1