I saw someone on Mastodon say they used ChatGPT to do this, so I took the hour it would take to do it right in F# and did that.

100% guaranteed to be correct or your money back (assuming you're using vanilla SRT files), and the value-added bonus of not having to ship things off to a billion dollar company to do simple string manipulation.

;) 

Yes, I know the `sed` command is much, much shorter.
